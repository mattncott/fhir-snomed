 FHIR-SNOMED Web Service Implementation
========================================


REST API that takes both Lincus and FHIR Observation data and converts to the corresponding format. Powered by PHP
with Curl and OpenSSL extensions. Also utilises Codeception for Unit Testing. 

REST API for connecting to SNOMED CT and returning relevant data to the user in a JSON format.
Powered by PHP with Curl Extensions and codeception for Unit Testing to ensure software runs correctly.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing 
purposes.

### Prerequisites

To get the REST Web Service running on a Server, ensure that the Server has the following extensions installed:

* OpenSSL - Required Extension for encrypting data for transmission
* cURL - Required Extension for connecting to SNOMED CT API
* Codeception - Software for performing unit tests on the Web Service

### How to Install

This section will outline installing the REST API to your system.

If using XAMPP, ensure that project folder is saved in:
```
../xampp/htdocs
```
Otherwise ensure the project is placed into correct directory, normally WWW for PHP.

### Accessing the Service to convert to a FHIR Structure
You are able to access this RESTful webservice using a client such as Advanced Rest Client. To do this,
input the following URL: ```http://[SERVER_NAME]/snomed/index.php?convert=lincus```
This URL will access the RESTful webservice with the intention to convert Lincus JSON into a FHIR compliant 
JSON message.

The Content type needs to be set to ```application/json```

This can be done by sending a POST request which will POST a JSON message with the following format:
```json
{
  "data": {
    "measurements": [
      {
        "type": "string(integer)",
        "value": "string(double)",
        "timestamp": "Date/Time",
        "date": "Date",
        "time": "Time",
        "timezone_offset": "+Time",
        "comments": "string",
        "userID": "string(integer)",
        "user_forename": "string",
        "user_surname": "string",
        "editID": "string(integer)",
        "support_forename": "string",
        "support_surname": "string",
        "device_id": "integer",
        "device_name": "string"
      },
      //below is an example from the above structure
      {
        "type": "1",
        "value": "75.3",
        "timestamp": "2017-02-03 13:35:59",
        "date": "2017-02-03",
        "time": "12:35:00",
        "timezone_offset": "+01:00",
        "comments": "",
        "userID": "11377",
        "user_forename": "forename2",
        "user_surname": "surname2",
        "editID": "11268",
        "support_forename": "support_forename",
        "support_surname": "support_surname",
        "device_id": null,
        "device_name": ""
      }
    ]
  }
}
```
The RESTful Webservice will convert this using FHIR guidelines to create a FHIR bundle resource. The bundle will contain
multiple FHIR observations. The following following is required in an observation: "Code, resourceType"
The Bundle will contain each measurement that has been fetched from Lincus. The system will read the relevant data from
the POST message and then will place this data into the corresponding parameter of the FHIR bundle message. 

Patient and performer data is derived from the Lincus message. The Patient ID is derived from the userID field. The 
user's name is then taken from the user_forename and concatenated with the user_surname. The support fields will be taken
and used to create the performer information of the FHIR message. To get the support forename and surname from the Lincus
message, you are required to reference the support_forename and support_surname. If either the patient's or performer's 
forename or surname are missing, then the fields will be missed out on the FHIR message as they are not a mandatory field.

If a device was used to record the measurement, then a device will be added to the FHIR message to display the device ID
and the Device name that was used to record the message. For example if the device tremend_IOT was used with an ID of 
10000, then the following JSON structure will be added to the FHIR structure:
```json
{
  "Device": {
      "reference": "Device/10000",
      "display" : "tremend_IOT"
  }
}
```

If no device is present or device_id is set to null in the Lincus message, then the device section will be left out of the
FHIR structure as it is not mandatory and will not be relevant to the message.

### Accessing the Service to convert to Lincus
#### Converting data to Lincus GET Data
Using this service, it is also possible to convert a Measurement with a FHIR compliant structure into a message structure
that is compatible with the Lincus Platform. To do this, access the web service with the following
URL ```http://[SERVER_NAME]/snomed/index.php?convert=fhir```

#### FHIR Data
You can then POST to the Web Service using the FHIR Data with the following format:
```json
{
  "resourceType": "Bundle",
  "type": "batch-response",
  "meta": {
    "lastUpdated": "2017-10-03T16:45:00+00:00"
  },
  "entry": [
    {
      "resource": {
        "resourceType": "Observation",
        "id": "156199",
        "identifier": [
          {
            "use": "official",
            "system": "http://hl7.org/fhir/identifier-use",
            "value": "226715088"
          }
        ],
        "status": "final",
        "code": {
          "coding": {
            "system": "http://snomed.info/sct/",
            "code": "27113001",
            "display": "Body weight"
          }
        },
        "subject": {
          "reference": "Patient/11376",
          "display": "forename surname"
        },
        "effectivePeriod": {
          "start": "2017-02-02T17:45:00+01:00"
        },
        "issued": "2017-02-02T16:45:00+00:00",
        "performer": [
          {
            "reference": "Performer/11376",
            "display": "forename surname"
          }
        ],
        "comment": "null",
        "valueQuantity": {
          "value": 76,
          "unit": "kg",
          "system": "http://snomed.info/sct/",
          "code": "258683005"
        },
        "interpretation": {
          "coding": [
            {
              "system": "http://hl7.org/fhir/v2/0078",
              "code": "N",
              "display": "Normal"
            }
          ]
        },
        "referenceRange": [
          {
            "low": {
              "value": 50,
              "unit": "kg (qualifier value)",
              "system": "http://snomed.info/sct/",
              "code": "258683005"
            },
            "high": {
              "value": 100,
              "unit": "kg (qualifier value)",
              "system": "http://snomed.info/sct/",
              "code": "258683005"
            }
          }
        ],
        "device": {
          "reference": "Device/1456",
          "display": "Medisana Glucometer XT506"
        }
      },
      "fullUrl": "lincus.eu/226715088"
    },
    {
      "resource": {
        "resourceType": "Observation",
        "id": "156199",
        "identifier": [
          {
            "use": "official",
            "system": "http://hl7.org/fhir/identifier-use",
            "value": "135559082"
          }
        ],
        "status": "final",
        "code": {
          "coding": {
            "system": "http://snomed.info/sct/",
            "code": "27113001",
            "display": "Body weight"
          }
        },
        "subject": {
          "reference": "Patient/11377",
          "display": "forename2 surname2"
        },
        "effectivePeriod": {
          "start": "2017-02-03T13:35:00+01:00"
        },
        "issued": "2017-02-03T13:35:59+00:00",
        "performer": [
          {
            "reference": "Performer/11268",
            "display": "support_forename support_surname"
          }
        ],
        "comment": "null",
        "valueQuantity": {
          "value": 75.3,
          "unit": "kg",
          "system": "http://snomed.info/sct/",
          "code": "258683005"
        },
        "interpretation": {
          "coding": [
            {
              "system": "http://hl7.org/fhir/v2/0078",
              "code": "N",
              "display": "Normal"
            }
          ]
        },
        "referenceRange": [
          {
            "low": {
              "value": 50,
              "unit": "kg (qualifier value)",
              "system": "http://snomed.info/sct/",
              "code": "258683005"
            },
            "high": {
              "value": 100,
              "unit": "kg (qualifier value)",
              "system": "http://snomed.info/sct/",
              "code": "258683005"
            }
          }
        ]
      },
      "fullUrl": "lincus.eu/135559082"
    }
  ]
}
```
Then it will convert it to a Lincus Compatible JSON message. Once again, this can be accessed using a program
like Advanced Rest Client. If you set the request to perform a POST request with a content type of 
```Content-Type: application/json``` then submit the above JSON, you should get a result which is compatible
with the Lincus Platform. 

### Install OpenSSL
Included in Server Installations such as XAMPP and WAMP, only requires OpenSSL to be enabled in php.ini:

```
Remove ;
;extension=php_openssl.dll
```

If you are not using a Server which comes packaged with OpenSSL, then please follow instructions found [here](https://secure.php.net/manual/en/openssl.installation.php).

### Install cURL

* [Download](https://curl.haxx.se/download.html)
* Unzip
* If using SSL then download OpenSSL DLLs available from cURL

Using a server such as XAMPP or WAMP, cURL comes preloaded. It will need enabling in the 
file php.ini. To do this just uncomment the line (remove ;):

```
;extension=php_curl.dll
```

### Install Codeception
#### Using Composer

Run in a command line interface: 
```
php composer.phar require "codeception/codeception:*"
```
Codecept can then be run by:
```
Navigate to directory 'vendor/bin'

Execute command 'php codecept run --steps'
```
#### Phar
Alternatively, you can download codeception directly from:
```
http://codeception.com/codecept.phar
```

## Running the unit tests
Ensure that the url of the server is pointing to the correct location. To check:
```
Open file 'vendor/bin/codeception.yml'

Under 'url:', check it is the correct server and directory location. (Default is https://[SERVER_NAME]/snomed)
```

To run unit tests, navigate to project folder and run (using Command Line Interface):
```
Examples using Windows CMD

cd vendor/bin

codecept run --steps 
    or run
codecept run
```
So far, the following tests have been created:

Test Name            | Tests 
-------------        | ------------- 
CheckJsonStructure   | Checks to ensure that the JSON returned from the Web Service is the correct format.
CorrectMeasurement   | Checks to ensure that if invalid data is sent to the Web Service, then the correct error is returned in the header.
TestJsonError        | Checks to ensure that if erroneous data is sent to the Web Service, then it will return the correct error response in the header message and in the JSON response.
TestNotAuthenticated | Tests to ensure that if an incorrect hash or no hash is sent to the webservice when accessing, then the user is rejected and the correct errors are returned.
WrongURLTest         | Tests to ensure that if the URL is incorrectly formed when attempting to access the Web Service, then the correct error messages are created and returned.
BothConversions      | Performs two conversions, this converts Lincus GET Data into a FHIR message and then converts it back into a Lincus GET message and checks to see it is correct. 
ConvertToLincus      | Converts a FHIR message into a Lincus GET message and checks to see the format of the data is correct
ValidateFhir         | This test converts a Lincus measurement into a FHIR structure. The structure will then be tested against the HAPI FHIR validator for a Bundle resource.

[How to test using Codeception](http://codeception.com/docs/10-WebServices#REST)

## Built Using

* [XAMPP](https://www.apachefriends.org/index.html) - Apache Server Developed on
* [Codeception](http://codeception.com/) - Unit Testing for REST API's
* [cURL](http://php.net/manual/en/book.curl.php) - Used to connect to SNOMED CT
* [openssl](https://www.openssl.org/) - Used to encrypt/decrypt all data

## Authors

* **Matthew Nethercott** - *Initial work*