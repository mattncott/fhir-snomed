<?php
/**
 * <h1>Encrypt</h1>
 * <br>
 * This class handles all of the encryption functions and data for the Web Service. Before the data is returned to the
 * calling application, this class ensures that the data is Encrypted using a OpenSSL and a key which is generated
 * by this web service to encrypt the data. This key is then saved on the Server so that it can be accessed by the calling
 * application to decrypt the data. This key is of a length of 4096KB which ensures that it is difficult to break.
 * <br>
 * The json message is entered into the function encryptData which handles whether new keys need generating. This class
 * can then only access the public key for the encryption, it does not have access to the private key, unless creating it.
 * <br>
 *
 * @author Matthew Nethercott
 * @version 1.0
 * @since 28 July 2017
 */
/**
 * Algorithm used to encrypt the data for transmission
 * @var string
 */
//define('ALGORITHM', 'aes-256-cbc');
/**
 * Location of the Key to encrypt/decrypt the data for
 * transmission
 * @var string
 */
//define('LOCATION', 'C://xampp/pri_key.key');
class Encrypt{
    const LOCATION = 'C://xampp/pri_key.key';
    const ALGORITHM = 'aes-256-cbc';
    /**
     * public key used to encrypt the message
     * @var string
     */
    private static $key;

    /**
     * <h2>createKey</h2>
     * <br>
     * This function creates the key required to encrypt/decrypt the message for transmission. The key is currently
     * a set of random pseudo_bytes with a length of 1024. This allows for greater security. The key is then saved
     * in the location specified in the constant LOCATION. This location needs to be accessible to the calling
     * application and this web service. Unless you are able to extract the file and store it with the calling
     * application.
     */
    public static function createKey(){
        $encryption_key = openssl_random_pseudo_bytes(1024);
        file_put_contents(self::LOCATION, $encryption_key);
    }

    /**
     * <h2>encryptData</h2>
     * <br>
     * This function encrypts the data once the key has been read and/or created. The key is then used to encrypt
     * the data. A non-NULL initialisation vector is applied to the encrypted and this is sent along with the encrypted
     * message. This must be extracted out of the data when decrypting the message.
     * @param $json_to_encrypt string json to encrypt and send to the calling application
     * @return string|null encrypted string or null if an error occurred when encrypting the data.
     */
    public static function encryptData($json_to_encrypt){
        self::loadKey();
        $iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length(self::ALGORITHM));

        $encrypted = openssl_encrypt($json_to_encrypt, self::ALGORITHM, Encrypt::$key, 0, $iv);
        //using a base64 encoding to the random IV prevents the nonzero chance of it containing a : which will
        //break when exploding the string on the client side.
        $encrypted = $encrypted . ":" . base64_encode($iv);

        return $encrypted;
    }

    /**
     * <h2>loadKey</h2>
     * <br>
     * Function checks to ensure that the key has been created to encrypt and decrypt the data. If the key has been
     * created, then the key is loaded into the global variable $key. If the file containing the key cannot be found,
     * then the function createKey is called.
     */
    private static function loadKey(){
        if (!file_exists(self::LOCATION)){
            Encrypt::createKey();
        }
        Encrypt::$key = file_get_contents(self::LOCATION);
    }
}