<?php
require_once ('Encrypt.php');
/**
 * <h1>Decrypt</h1>
 * <br>
 * This class handles all encrypted Data to ensure that it is decrypted ready to be
 * handled by the API. Encrypted data is passed to the static method decryptData. This
 * class then returns a Decrypted String back to the calling method. To ensure that a key
 * has been created by the system, decryptData calls the private method loadKey(). This
 * method checks to ensure that a key has been created and if not calls the class Encrypt.php
 * to make a key.
 * @author Matthew Nethercott
 * @version 1.0
 * @since 31/07/2017
 */
class Decrypt{
    /**
     * <h2>decryptData</h2>
     * <br>
     * This method accepts an encrypted string of data and returns its decrypted value.
     * Data is decrypted using the same key it was encrypted with and removes the vi in this
     * method. This method will call the static method loadKey to check if a key is created and
     * load it.
     * @param $data_to_decrypt
     * @return string
     */
    public static function decryptData($data_to_decrypt){
        $key = self::loadKey();
        $parts = explode(':', $data_to_decrypt);

        $decrypted = openssl_decrypt($parts[0], Encrypt::ALGORITHM, $key, 0, base64_decode($parts[1]));
        return $decrypted;
    }

    /**
     * <h2>loadKey</h2>
     * <br>
     * Function checks to ensure that the key has been created to encrypt and decrypt the data. If the key has been
     * created, then the key is loaded into the global variable $key. If the file containing the key cannot be found,
     * then the function createKey is called.
     */
    private static function loadKey(){
        if (!file_exists(Encrypt::LOCATION)){
            Encrypt::createKey();
        }
        return file_get_contents(Encrypt::LOCATION);
    }
}