<?php
/**
 * <h1>index.php</h1>
 * <br>
 * This PHP file commences and finishes the execution of the conversion between a FHIR structure and a Lincus Compliant
 * structure. This class is first accessed when accessing the Web Service. This class will evaluate what type of
 * conversion is inteded to use by the value of the parameter $_GET["convert"].
 * <ul>
 * <li>If the value of $_GET["convert"] is "lincus", then the web servie will perform a conversion to convert the data into
 * a FHIR compliant bundle resource.</li>
 * <li>If the vlaue of $_GET["convert"] is equal to "fhir" the, it will accept a FHIR compliant bundle structure
 * with the intention to convert the observation data into a measurement message for use within the Lincus
 * platform. </li>
 * </ul>
 * <br>
 * The system will perform the conversion on a payload sent to the application. This payload <strong>MUST</strong>
 * be in the format Content-Type: application/json for the system to run correctly. The system will also return
 * data in this format.
 * <br>
 * If an error occurs during the execution of the conversion, then an appropriate error message will be returned to the
 * calling application in a FHIR format. This format will be of an OperationOutcome message.
 * <br>
 * @author Matthew Nethercott
 * @version 1.0
 * @since 13th July 2017
 */

//include classes
require_once 'Encryption/Encrypt.php';
require_once 'Encryption/Decrypt.php';
require_once 'Rest/Conversion.php';
require_once 'Rest/ErrorMessage.php';

/**
 * @var string UTC defines the value of a UTC timezone_offset
 */
define ("UTC", "0");

$view = '';

$response_code_descriptions = array(
    404 => 'Not Found',
    408 => 'Request Time Out',
    500 => 'Internal Server Error'
);

if(isset($_GET['convert'])){
    $view = $_GET['convert'];
}

//check what view equals and return the corresponding JSON message to the querying application
$convert = new Conversion();

//Ensure that the conversion type is in lower case, removes any ambiguous entries
$view = strtolower($view);

switch($view) {

    /**
     * case 'lincus'
     * This case is used when converting Data from the Lincus format into a FHIR compliant bundle response
     * A JSON message from the Lincus API is accepted
     *
     */
    case 'lincus':
        header('Content-type: application/json');
        //get the JSON message posted to the web service
        $json = json_decode(file_get_contents('php://input'), true);
        /**
         * The array that will be constructed and returned to the calling application
         */
        $json_to_return = array();

        $time_list = array();

        if(json_last_error() !== JSON_ERROR_NONE){
        //if ($json === null){
            http_response_code(500);
            $json_structure = ErrorMessage::generateError('HTTP Error 500 Internal Server Error, Invalid JSON'
                .' Structure Error is: ' . json_last_error_msg(), ErrorCodes::INVALID);
        }else {
            foreach ($json["data"]["measurements"] as $measurement) {
                //Get relevant data from the Lincus Message to convert into the FHIR message
                $patient = $measurement[JsonElements::USER];
                $author = $measurement[JsonElements::EDIT];
                $type = $measurement[JsonElements::TYPE];
                $timezone = $measurement[JsonElements::TIMESTAMP];
                $value = $measurement[JsonElements::VALUE];
                $comment = $measurement[JsonElements::COMMENTS];
                $device_name = $measurement[JsonElements::DEVICE_NAME];
                $device_id = $measurement[JsonElements::DEVICE_ID];
                $offset = $measurement[JsonElements::TIMEZONE];
                $date = $measurement[JsonElements::DATE];
                $time = $measurement[JsonElements::TIME];
                $forename = $measurement[JsonElements::U_FORENAME];
                $surname = $measurement[JsonElements::U_SURNAME];
                $edit_forename = $measurement[JsonElements::S_FORENAME];
                $edit_surname = $measurement[JsonElements::S_SURNAME];
                //end of Lincus Fetch

                $device = array($device_id, $device_name);


                //convert the UTC format into a FHIR compliant format
                if ($offset === UTC)
                    $offset = "+00:00";


                $start = $date . "T" . $time . $offset;
                //add all the times from the Lincus Message into an array to get last updated time later
                array_push($time_list, $start);

                $organisation = $a_organisation = null;

                //save the forenames in an array, easier to send to a function to construct the message
                $forenames = array($forename, $edit_forename);
                //save the surnames in an array
                $surnames = array($surname, $edit_surname);
                $organisations = array($organisation, $a_organisation);

                $times = array($timezone, $start);

                //construct the message with the data above and save into an array called Raw Data
                $raw_data = $convert->getLincusConversion($type, $patient, $author, $value, $forenames, $surnames, $times,
                    $device, $offset, $comment);
                //if no data was returned, then an error must've occurred. Check header to see what happened
                if (is_null($raw_data)) {
                    $code = null;
                    switch (http_response_code()) {
                        case 404:
                            $code = ErrorCodes::NOT_FOUND;
                            break;
                        case 408:
                            $code = ErrorCodes::TIMEOUT;
                            break;
                        case 500:
                            $code = ErrorCodes::CODE_INVALID;
                            break;
                        default :
                            $code = ErrorCodes::EXCEPTION;
                            break;
                    }
                    //if an error has occurred then, construct the FHIR error message and log the error on the system
                    if (array_key_exists(http_response_code(), $response_code_descriptions)) {
                        $raw_data = ErrorMessage::generateError('HTTP Error ' . http_response_code()
                            . ': ' . $response_code_descriptions[http_response_code()] . ', please see'
                            . ' the log files for more information', $code);
                    } else {
                        //else, an unknown error may have occurred, construct this message
                        $raw_data = ErrorMessage::generateError('An error has occurred, please'
                            . 'see the logs for more information.', ErrorCodes::UNKNOWN);
                    }
                }
                //push this to the array json_to_return and up the length
                array_push($json_to_return, $raw_data);
            }
            //sort the list of times to get the last updated time
            rsort($time_list);

            //The structure of a bundle message, must be used for multiple measurements. Easier to just return on all measurements
            $json_structure = array(
                "resourceType" => "Bundle",
                "type" => "batch-response",
                "meta" => array(
                    "lastUpdated" => $time_list[0]
                ),
                "entry" => array(array(
                    "resource" => "JSON"
                ))
            );

            //the entry
            $i = 0;
            //foreach measurement entry in json to return, get the identifier and apply to the fullUrl for the bundle resource
            foreach ($json_to_return as $item) {
                $identifier = array();
                //get the identifier
                if (array_key_exists("identifier", $item)) {
                    $identifier = $item["identifier"][0];
                }
                //get the value of the identifier
                if (array_key_exists("value", $identifier)) {
                    $id = $identifier["value"];
                } else {
                    $id = rand(1, 999999999);
                }
                //add the measurement to the resource for the bundle, and apply the identifier to the fullUrl
                $json_structure["entry"][$i]["resource"] = $item;
                $json_structure["entry"][$i]["fullUrl"] = "lincus.eu/$id";
                $i++;
            }
        }
        $response = $json_structure;
        break;

    /**
     * case 'fhir_get'
     * This case, will convert the FHIR data into a message that resembles the message displayed if you
     * access the Lincus API to get data from the API.
     */
    case 'fhir':
        header('Content-type: application/json');
        $response =  "Accepts FHIR data to convert into Lincus";
        $json = json_decode(file_get_contents("php://input"), true);

        //Arrays used for structure
        /**
         * The final message to be constructed and returned to the user
         */
        $json_to_return = array();
        /**
         * Array of users to be used to construct user part of Lincus Message
         */
        $user_array = array();
        /**
         * Temporary array used to check no duplicate users are stored
         */
        $user_array_temp = array();
        /**
         * Array of user_ids found in the FHIR message, used to construct user_id section of Lincus message
         */
        $user_ids = array();

        $org_member_array = array();

        if ($json === null){
            http_response_code(500);
            $json_to_return = ErrorMessage::generateError('HTTP Error 500 Internal Server Error, Invalid JSON'
                .' Structure Error is: ' . json_last_error_msg(), ErrorCodes::INVALID);
        }else {
            foreach ($json["entry"] as $measurement) {
                //Get relevant data from the FHIR message
                //TODO Check if these actually exist as they are not mandatory
                $measurement = $measurement["resource"];
                $type = $measurement[JsonElements::CODE]["coding"]["code"];
                $measurement_name = $measurement[JsonElements::CODE]["coding"]["display"];
                $timestamp = $measurement[JsonElements::ISSUED]; //SERVER time
                $user = $measurement[JsonElements::SUBJECT]["reference"];
                $edit = $measurement[JsonElements::PERFORMER][0]["reference"];
                $value = $measurement[JsonElements::VALUE_QUANTITY]["value"];
                $unit = $measurement[JsonElements::VALUE_QUANTITY]["unit"];
                $comment = $measurement[JsonElements::COMMENT];
                $date = $measurement[JsonElements::PERIOD]["start"]; //Created Time
                $user_details = $measurement[JsonElements::SUBJECT];
                $support_details = $measurement[JsonElements::PERFORMER][0];

                $device_name = null;
                $device_id = null;
                if (array_key_exists("device", $measurement)) {
                    $device_name = $measurement["device"]["display"];
                    $device_id = substr($measurement["device"]["reference"],
                        strpos($measurement["device"]["reference"], "/") + 1,
                        (strlen($measurement["device"]["reference"])));
                }

                $device = array($device_name, $device_id);
                //End of reading from FHIR message

                $raw_data = $convert->getFhirGetConversion($type, $timestamp, $value, $user, $user_details,
                    $support_details, $edit, $device, $date, $comment);

                //add the current measurement to the final structure
                array_push($json_to_return, $raw_data);
            }

            $json_to_return = array(
                "data" => array(
                    "measurements" => $json_to_return
                )
            );
        }

        $response = $json_to_return;
        break;

    /**
     * default case
     * If none of the above cases are set, then an error must've occurred in the url therefore,
     * return an error message in a JSON format to the calling application.
     */
    default:
        http_response_code(404);
        header("Content-Type: application/json");
        $response = ErrorMessage::generateError("HTTP Error 404: Not Found, An Incorrect URL has been used"
            , ErrorCodes::NOT_FOUND);
        Conversion::logError("Incorrect URL formation", $_SERVER["REQUEST_URI"]);
        break;
}

$vars = array_keys(get_defined_vars());

//unset all declared variables
for ($i = 0; $i < sizeof($vars); $i++){
    unset($vars[$i]);
}
unset($vars,$i);

//return the JSON to the calling application
echo json_encode($response);