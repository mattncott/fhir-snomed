<?php
require_once 'Enum/ErrorCodes.php';
/**
 * <h1>ErrorMessage</h1>
 * <br>
 * This class handles returning a FHIR compliant error message
 * back to the calling class so that a compliant error message is
 * returned back to the user. The Structure of an OperationOutcome is
 * used to build the error message and an Enumeration called ErrorCodes
 * is accessed to display the correct code to the user.
 * @author Matthew Nethercott
 * @version 1.0
 * @since 23 August 2017
 */

final class ErrorMessage{
    const RESOURCE = "resourceType";
    const ID = "id";
    const ISSUE = "issue";
    const MESSAGE_STRUCTURE= array(
        self::RESOURCE => "OperationOutcome",
        self::ID => "144371",
        self::ISSUE => array(array(
            "severity" => "error",
            "code" => "exception",
            "details" => array(
                "text" => "Populate from program"
            )
        ))
    );

    /**
     * <h2>generateError</h2>
     * <br>
     * This function, populates the error message from the structure defined in MESSAGE_STRUCTURE
     * with an error message and the correct FHIR code for the type of error that has occurred.
     * This error message is then returned back to the calling method and returned to the calling system.
     * @param $error string the error message to enter in to the JSON response
     * @param $error_code string taken from the Enumeration Enum/ErrorCodes
     * @return array FHIR Compliant Error Message
     */
    public static function generateError($error, $error_code = ErrorCodes::EXCEPTION){
        $error_message = self::MESSAGE_STRUCTURE;
        $error_message[self::ISSUE][0]["code"] = $error_code;
        $error_message[self::ISSUE][0]["details"]["text"] = $error;
        return $error_message;
    }
}