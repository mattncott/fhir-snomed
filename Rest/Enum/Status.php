<?php
/**
 * <h1>Status</h1>
 * <br>
 * Enumeration class for status to be held in the FHIR message.
 * The constant variables are taken from the Schema on HL7 for an Observation.
 * The enumeration structure is defined on there so will need to be updated and checked
 * with each revision of HL7.
 * <br>
 * It is still unsure however, if this enumeration will be needed for the Lincus integration
 * as it should be final when going to the Lincus database. However, it has been included
 * to conform with the FHIR Schema provided.
 * @author Matthew Nethercott
 * @version 1.0
 * @since 11 August 2017
 */
abstract class Status {
    const REGISTERED = "registered";
    const PRELIMINARY = "preliminary";
    const FINAL = "final";
    const AMENDED = "amended";
    const CORRECTED = "corrected";
    const CANCELLED = "cancelled";
    const IN_ERROR = "entered-in-error";
    const UNKNOWN = "unknown";
};