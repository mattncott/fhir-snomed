<?php
/**
 * <h1>ErrorCodes</h1>
 * <br>
 * Enumeration class for the types of errors that could be thrown
 * during the execution by this program. These constant variables have been
 * taken from the HL7 schema for an OperationOutcome and will be displayed
 * in the FHIR Compliant JSON message for an OperationOutcome
 * @author Matthew Nethercott
 * @version 1.0
 * @since 23 August 2017
 */

abstract class ErrorCodes {
    const INVALID = "invalid";
    const UNKNOWN = "unknown";
    const FORBIDDEN = "forbidden";
    const NOT_FOUND = "not-found";
    const CODE_INVALID = "code-invalid";
    const EXCEPTION = "exception";
    const TIMEOUT = "timeout";
}