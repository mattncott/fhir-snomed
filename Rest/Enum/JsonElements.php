<?php
/**
 * <h1>JsonElements</h1>
 * <br>
 * An enumeration class intended to hold the value for the tags used in both JSON Messages.
 * It will hold the parent tags for the messages. The children tags will need to be hard coded in
 * method creating the JSON.
 * <br>
 * The first set of constant variables are defined in the FHIR schema: Observation.schema. It will
 * need to be ensured that it is kept correct to the standards dictated in this schema. This class
 * will hold the relevant and mandatory elements needed for the FHIR message defined in the schema.
 * <br>
 * The second set of constants are defined in the Lincus JSON message. These have been agreed with Laura
 * for the structure of the message. The labels are defined in this class and a class will be created to
 * structure the message with the correct data.
 * @author Matthew Nethercott
 * @version 1.0
 * @since: 3 October 2017
 */

abstract class JsonElements {

    //FHIR JSON Labels
    const RESOURCE_TYPE = "resourceType";  //<-- Required
    const ID = "id";
    const STATUS = "status";
    const CATEGORY = "category";
    const CODE = "code"; //<-- Required
    const SUBJECT = "subject";
    const PERFORMER = "performer";
    const CONTEXT = "context";
    const ISSUED = "issued";
    const VALUE_QUANTITY = "valueQuantity";
    const IDENTIFIER = "identifier";
    const PERIOD = "effectivePeriod";
    const INTERPRETATION = "interpretation";
    const RANGE = "referenceRange";
    const COMMENT = "comment";


    //Lincus JSON Labels
    const TYPE = "type";
    const VALUE = "value";
    const TIMESTAMP = "timestamp";
    const DATE = "date";
    const TIME = "time";
    const TIMEZONE = "timezone_offset";
    const COMMENTS = "comments";
    const USER = "userID";
    const U_FORENAME = "user_forename";
    const U_SURNAME = "user_surname";
    const EDIT = "editID";
    const S_FORENAME = "support_forename";
    const S_SURNAME = "support_surname";
    const DEVICE_ID = "device_id";
    const DEVICE_NAME = "device_name";
}