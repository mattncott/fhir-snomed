<?php
/**
 * <h1>Identifier</h1>
 * <br>
 * Enumeration class for Identifiers to be held in the FHIR message.
 * These constant variables are taken from the HL7 Schema for an Identifier.
 * The enumeration variables are defined by HL7 so will need to be maintained according
 * to their schemas.
 * @author Matthew Nethercott
 * @version 1.0
 * @since 15 August 2017
 */

abstract class Identifier {
    const USUAL = "usual";
    const OFFICIAL = "official";
    const TEMP = "temp";
    const SECONDARY = "secondary";
}