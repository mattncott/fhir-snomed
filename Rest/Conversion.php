<?php

//Real World example of FHIR Observation: https://www.hl7.org/fhir/observation-example-f005-hemoglobin.json.html
require_once 'Enum/Status.php';
require_once 'Enum/Identifier.php';
require_once 'Enum/JsonElements.php';

//Constant variables for connecting to snomed to search on International Edition
define('EDITION', 'en-edition/');
define('RELEASE', 'v20180131/'); //<-- This variable is likely to change with new releases of SNOMED
define('SNOMED_SYSTEM', 'http://snomed.info/sct/');

//How long to wait for a response from an API until declaring a timeout. In ms
define('TIMEOUT', 3);
/**
 * Complete url formation of the SNOMED API, just append an SCTID to access
 */
define('BASE_URL', 'http://browser.ihtsdotools.org/api/snomed/'.EDITION.RELEASE.'concepts/');
/**
 * <h1>Conversion</h1>
 * <br>
 * Handles connecting to SNOMED CT API. Once connected and received JSON response from the server, then the data is
 * interpreted and the correct value is placed into the correct position in the array $json_message. This JSON String
 * (provided, that no Internal Server Errors Occurred) is then returned to the calling class and encoded to be sent
 * to the querying application.
 * @author Matthew Nethercott
 * @version 1.0
 * @since 18 July 2017
 */
final class Conversion {
    /* Constant variables used to construct the JSON message.
     * These variables are used to define the main keys for the
     * message. To define the children keys, then change the strings
     * in the JSON structure.
     */

    /**
     * Has an error occurred during the process of populating the messages
     * @var bool false|true as to whether an error has occurred
     */
    private $error = false;

    /**
     * Default Json Lincus Message returned to the user. This is changed in the code.
     * The structure of this message has been constructed using the Lincus API.
     */
    const lincus_message = array(
        JsonElements::TYPE => "",
        JsonElements::VALUE => "",
        JsonElements::TIMESTAMP => "",
        JsonElements::DATE => "",
        JsonElements::TIME => "",
        JsonElements::TIMEZONE => "",
        JsonElements::COMMENTS => "",
        JsonElements::USER => "",
        JsonElements::U_FORENAME => "",
        JsonElements::U_SURNAME => "",
        JsonElements::EDIT => "",
        JsonElements::S_FORENAME => "",
        JsonElements::S_SURNAME => "",
        JsonElements::DEVICE_ID => null,
        JsonElements::DEVICE_NAME => ""
    );

    /**
     * Default JSON FHIR message returned to the user. Will be changed in the code.
     * The structure of this message has been constructed using Real World examples
     * demonstrated on the examples section for an observation on the HL7 website.
     */
    const fhir_message = array(
        JsonElements::RESOURCE_TYPE => "What is this resource",
        JsonElements::ID => "Random identifier for the message", //Random value to identify the measurement
        JsonElements::IDENTIFIER => array(array(
            "use" => "The purpose of the identifier",
            "system" => "Establishes namespace for the value - a URL that describes a set values that are unique",
            "value" => "The portion of identifier typically relevant to the user"
        )),
        JsonElements::STATUS => "The status of the result value",
        JsonElements::CODE => array(
            "coding" => array(
                "system" => "The identification of teh code system that defines the meaning of the symbol in the code",
                "code" => "A symbol in syntax defined by the system",
                "display" => "A representation of the meaning of teh code in the system, following rules of the system"
            )
        ),
        JsonElements::SUBJECT => array(
            "reference" => "The patient, group of patients, where the observation was made",
            //"display" => "Plain text narrative explaining the reference"
        ),
        JsonElements::PERIOD => array(
            "start" => "The start of the period. The boundary in inclusive",
            //End will need to be added if there is an end date, see
        ),
        JsonElements::ISSUED => "The date and time this observation was made available to providers",
        JsonElements::PERFORMER => array(array(
            "reference" => "The patient, group of patients, where the observation was made",
            //"display" => "Plain text narrative explaining the reference"
        )),
        JsonElements::COMMENT => "null",
        JsonElements::VALUE_QUANTITY => array(
            "value" => "measured amount or amount potentially measured", //int value eg the measurement
            "unit" => "readable form of the unit (EG: kg)",
            "system" => "The identification of the system that provides the coded form of the unit",
            "code" => "A computer processable form of the unit in some unit representation"
        ),
        JsonElements::INTERPRETATION => array(
            "coding" => array(array(
                "system" => "The identification of teh code system that defines the meaning of the symbol in the code",
                "code" => "A symbol in syntax defined by the system",
                "display" => "A representation of the meaning of teh code in the system, following rules of the system"
            ))
        ),
        JsonElements::RANGE => array(array(
            "low" => array(
                "value" => "Measured amount or amount potentially measured",
                "unit" => "readable form of the unit (EG:kg)",
                "system" => "The identification of the system that provides the coded form of the unit",
                "code" => "A computer processable form of the unit in some unit representation"
            ),
            "high" => array(
                "value" => "Measured amount or amount potentially measured",
                "unit" => "readable form of the unit (EG:kg)",
                "system" => "The identification of the system that provides the coded form of the unit",
                "code" => "A computer processable form of the unit in some unit representation"
            )
        ))
    );

    /**
     * SCTID Values of measurements that can be used to search for the corresponding measurement
     * in the SNOMED CT API. Each SCTID has corresponding measurement commented next to it.
     * @var array
     */
    private $measurements = array(
        1 =>  '27113001',      //'Weight',
        2 =>  '448389006',     //'Pedometer',
        3 =>  '78564009',      //'Pulse',
        4 =>  '444981005',     //'Resting Pulse',
        5 =>  '271649006',     //'Systolic Blood Pressure'
        6 =>  '271650006',     //'Diastolic Blood Pressure',
        7 =>  '386725007',     //'Temperature',
        8 =>  '103228002',     //'Oxygen Saturation',
        9 =>  '313193002',     //'Peak Flow',
        10 => '310520004',     //'FEV1',
        11 => '365812005',     //'Blood Glucose',
        12 => '251834001',     //Energy Burned TODO find energy burned currently Total Energy Expenditure
        13 => '246132006',     //Distance
        14 => '255456001',     //Elevation
        15 => '313835008',     //'HBA1C',
        16 => '84698008',      //'Cholesterol',
        17 => '276361009',     //'Waist Circumference',
        18 => '248263006',     //'Hours of Sleep'
    );

    /**
     * Contains the units to match the measurements in the array above. For instance, measurement 5,
     * Systolic Blood Pressure will match to the unit "mmHg". Each measurement position in the array,
     * must match the position in the array $units.
     * @var array
     */
    private $units = array(
        1 => array(  //Kilograms
            "code" => '258683005',
            "term_number" => 2
        ),
        2 => array(  //Step
            "code" => '398298007',
            "term_number" => 1
        ),
        3 => array(  //BPM
            "code" => '258983007',
            "term_number" => 2
        ),
        4 => array(  //BPM
            "code" => '258983007',
            'term_number' => 2
        ),
        5 => array(  //mmHg
            "code" => '259018001',
            "term_number" => 3
        ),
        6 => array(  //mmHg
            'code' => '259018001',
            'term_number' => 3
        ),
        7 => array(  //Celsius
            'code' => '258710007',
            'term_number' => 0
        ),
        8 => array(  //% (Percentage)
            'code' => '258752002',
            'term_number' => 0
        ),
        9 => array(  //Liter/minute
            'code' => '258994006',
            'term_number' => 0
        ),
        10 => array(  //%VC
            'code' => '268379003',
            'term_number' => 2
        ),
        11 => array(  //mmol/L
            'code' => '258813002',
            'term_number' => 5
        ),
        12 => array(  //kcal
            'code' => '258791007',
            'term_number' => 2
        ),
        13 => array(  //m
            'code' => '258669008',
            'term_number' => 0
        ),
        14 => array(  //m
            'code' => '258669008',
            'term_number' => 0
        ),
        15 => array(  //%
            'code' => '415067009',
            'term_number' => 0
        ),
        16 => array(  //mmol/L
            'code' => '258813002',
            'term_number' => 5
        ),
        17 => array(  //cm
            'code' => '258672001',
            'term_number' => 2
        ),
        18 => array(  //hour
            'code' => '258702006',
            'term_number' => 2
        ),
    );

    /**
     * Holds the values of the upper and lower boundaries for the values of each measurement.
     * These will be compared in a referencing function and then they will be displayed in the
     * JSON message.
     * <br>
     * Should be displayed in the self::referenceRange section of the FHIR JSON Message.
     * @var array
     */
    private $levels = array(
        1 => array(     //Weight
            "low" => "50",
            "high" => "100"
        ),
        2 => array(     //Pedometer
            "low" => "9500",
            "high" => "10500"
        ),
        3 => array(     //Pulse
            "low" => "60",
            "high" => "80"
        ),
        4 => array(     //Resting Pulse
            "low" => "50",
            "high" => "70"
        ),
        5 => array(     //Systolic BP
            "low" => "90",
            "high" => "120"
        ),
        6 => array(     //Diastolic BP
            "low" => "60",
            "high" => "80"
        ),
        7 => array(     //Temperature
            "low" => "33",
            "high" => "39"
        ),
        8 => array(     //Oxygen Saturation
            "low" => "90",
            "high" => "100"
        ),
        9 => array(     //Peak Flow
            "low" => "400",
            "high" => "650"
        ),
        10 => array(    //FEV1
            "low" => "2",
            "high" => "5"
        ),
        11 => array(    //Blood Glucose
            "low" => "3.9",
            "high" => "7.1"
        ),
        12 => array(    //Energy Burned
            "low" => "1000",
            "high" => "3000"
        ),
        13 => array(    //Distance
            "low" => "0",
            "high" => "8"
        ),
        14 => array(    //Elevation
            "low" => "0",
            "high" => "50"
        ),
        15 => array(    //HBA1c
            "low" => "4",
            "high" => "14"
        ),
        16 => array(    //Cholesterol
            "low" => "3",
            "high" => "12"
        ),
        17 => array(    //Waist Circumference
            "low" => "75",
            "high" => "95"
        ),
        18 => array(    //Hours of Sleep
            "low" => "6",
            "high" => "9"
        ),
    );

    /**
     * <h2>getLincusConversion</h2>
     * <br>
     * Converts a single measurement in the Lincus format into a FHIR measurment and returns the new structure to the
     * calling function
     * @param $measurement_id int value of the id number to fetch in the array, [1-15]
     * @param $patient int value of the id number to assign to the patient in the data
     * @param $author int value of the author to assign to this data
     * @param $value int value of the measurement sent from calling application
     * @param $forenames array forenames of the patient and the author respectively
     * @param $surnames array surnames of the patient and the author respectively
     * @param $times array start and end times that the measurement was captured respectively. End can be null
     * @param $device array the device that was used to record the measurement
     * @param $comment string value of a comment if there is one present
     * @return array JSON message created from the data captured.
     */
    public function getLincusConversion($measurement_id, $patient, $author, $value, $forenames, $surnames, $times,
                                  $device, $offset, $comment = "null") {

        //set the error variable to false, no errors have been found yet
        $this->error = false;

        //if the the patient or author is not an int value, then there has been an error
        if ((strval($patient) != strval(floatval($patient))) || (strval($author) != strval(floatval($author)))){
            $this->error = true;
            //set the headers
            http_response_code(500);
            header("HTTP/1.1 500 Internal Server Error: AuthorID and/or PatientID must be an INT");
            //log the error
            self::logError("Patient and Author must be an integer value", "$patient and $author");
        }
        //if the measurement value is not a float value, then there has been an error
        if ((strval($value) != (strval(floatval($value)))) && strval($value) != ""){
            $this->error = true;
            //set the headers
            http_response_code(500);
            header("HTTP/1.1 500 Internal Server Error: Value/Measurement must be an integer value");
            //log the error
            self::logError("Value/Measurement must be an integer value", $value);
        }

        //is data collection finished
        $exit = false;

        //what are we searching for on snomed
        $to_check = 'measurement';

        //Fetch API data from Cache or SNOMED Directly
        //Only exit loop if at end of data fetch or an error has occurred during the data collection
        while (!$this->error && !$exit){
            switch ($to_check){
                //check snomed for the measurement
                case 'measurement':
                    $measurement = $this->getMeasurement($measurement_id)['descriptions'][0]['term'];
                    $to_check = 'unit';
                    break;
                //check snomed for the unit value
                case 'unit':
                    $unit = $this->getUnit($measurement_id)['descriptions'][1]['term'];
                    $to_check = 'lang';
                    $exit = true;
                    break;
            }
        }

        //if an error has not occurred, then populate the message
        if (!$this->error) {

            //get the measurement code for the FHIR message
            $code = $this->measurements[$measurement_id];

            //check the cached files expiration time
            $this->checkFileTime();

            //construct the message
            //A warning will display saying $measurement and $unit are not defined, they are. Cannot get here without
            //them
            return $this->constructFhirMessage($code, $measurement_id, $measurement, $unit, $value, $author, $patient,
                $forenames, $surnames, $times, $device, $offset, $comment);
        }
        $this->checkFileTime();
        return null;
    }

    /**
     * <h2>getDefaultJsonStructure</h2>
     * <br>
     * Returns the default structure of the JSON message before any data has been changed in the message.
     * Tends to be used for debugging that the message is of the correct structure.
     * @return array structure of the JSON message
     */
    public function getDefaultJsonStructure(){
        return self::fhir_message;
    }

    /**
     * <h2>constructMessage</h2>
     * <br>
     * This function, takes the constant FHIR JSON Structure defined at the top of the class and
     * populates the structure with data already fetched from the JSON input. The data is interpreted
     * by this function and input into the corresponding locations in the JSON message.
     * @param $measurement_code string SNOMED CT Code for the measurement that has been captured
     * @param $measurement_id int type of measurement that has been captured. ID's from the Lincus API have been used
     * @param $measurement_display string human readable form of the measurement that has been captured
     * @param $unit_display string human readable form of the unit that the measurement uses
     * @param $value int reading taken for the corresponding measurement
     * @param $author string who wrote the measurement on the Lincus Website
     * @param $patient string who is the patient that the measurement has been taken on
     * @param $forenames array forenames of the patient and the author respectively (can be null)
     * @param $surnames array surnames of the patient and the author respectively (can be null)
     * @param $times array times that the data was captured (start and end time respectively) end can be null
     * @param $device array the device/app that was used to capture the measurement
     * @param $comment string a string comment if one is present when entered
     * @return array FHIR compliant JSON message.
     */
    private function constructFhirMessage($measurement_code, $measurement_id, $measurement_display, $unit_display, $value,
                                          $author, $patient, $forenames, $surnames, $times, $device, $offset, $comment) {
        $high = 'H';
        $low = 'L';
        $normal = 'N';

        //load into new Array so original is not edited, this is so that the structure of the message is always correct
        //otherwise the class has to be instantiated with every measurement taking up server memory
        $json_array = self::fhir_message;

        //self::RESOURCE_TYPE
        $json_array[JsonElements::RESOURCE_TYPE] = "Observation";

        //self::ID
        //This is the FHIR id value for an Observation
        $json_array[JsonElements::ID] = "156199";

        //self::IDENTIFIER
        //generate a unique id for this Observation
        $id = (string)rand(1, 999999999); //cast to string for FHIR compliance
        $json_array[JsonElements::IDENTIFIER][0]["use"] = Identifier::OFFICIAL;
        $json_array[JsonElements::IDENTIFIER][0]["system"] = "http://hl7.org/fhir/identifier-use";
        $json_array[JsonElements::IDENTIFIER][0]["value"] = $id;

        //self::STATUS
        //currently set to final as the message should be generated successfully from here
        $json_array[JsonElements::STATUS] = Status::FINAL;

        //self::CODE
        $json_array[JsonElements::CODE]["coding"]["system"] = SNOMED_SYSTEM;
        $json_array[JsonElements::CODE]["coding"]["code"] = $measurement_code;
        $json_array[JsonElements::CODE]["coding"]["display"] = $measurement_display;

        //self::SUBJECT
        $json_array[JsonElements::SUBJECT]["reference"] = "Patient/".$patient;
        //if their is a forname and a surname present, then populate the display attribute
        if ($forenames[0] != "" && $surnames[0] != ""){
            $json_array[JsonElements::SUBJECT]["display"] = "$forenames[0] $surnames[0]";
        }

        //self::PERIOD
        $json_array[JsonElements::PERIOD]["start"] = $times[1];
        //if end is set in LINCUS API, then set end in this system

        //self::ISSUED
        $json_array[JsonElements::ISSUED] = str_replace(" ", "T", $times[0]) . "+00:00";

        //self::PERFORMER
        $json_array[JsonElements::PERFORMER][0]["reference"] = "Performer/".$author;
        if ($forenames[1] != "" && $surnames[1] != ""){
            $json_array[JsonElements::PERFORMER][0]["display"] = "$forenames[1] $surnames[1]";
        }

        //self::VALUE_QUANTITY
        //make sure that is is a float value
        $json_array[JsonElements::VALUE_QUANTITY]["value"] = (float)$value;
        $json_array[JsonElements::VALUE_QUANTITY]["unit"] = $unit_display;
        $json_array[JsonElements::VALUE_QUANTITY]["system"] = SNOMED_SYSTEM;
        $json_array[JsonElements::VALUE_QUANTITY]["code"] = $this->units[$measurement_id]["code"];

        if (!is_null($device[0])){
            $json_array["device"]["reference"] = "Device/$device[0]";
            $json_array["device"]["display"] = $device[1];
        }

        //self::INTERPRETATION
        $hl7_interpretation_system = "http://hl7.org/fhir/v2/0078";
        if (array_key_exists($measurement_id, $this->levels)) {
            if ($value <= $this->levels[$measurement_id]["low"]){
                $json_array[JsonElements::INTERPRETATION]["coding"][0]["system"] = $hl7_interpretation_system;
                $json_array[JsonElements::INTERPRETATION]["coding"][0]["code"] = $low;
                $json_array[JsonElements::INTERPRETATION]["coding"][0]["display"] = "Low";
            } else if ($value >= $this->levels[$measurement_id]["high"]){
                $json_array[JsonElements::INTERPRETATION]["coding"][0]["system"] = $hl7_interpretation_system;
                $json_array[JsonElements::INTERPRETATION]["coding"][0]["code"] = $high;
                $json_array[JsonElements::INTERPRETATION]["coding"][0]["display"] = "High";
            } else {
                $json_array[JsonElements::INTERPRETATION]["coding"][0]["system"] = $hl7_interpretation_system;
                $json_array[JsonElements::INTERPRETATION]["coding"][0]["code"] = $normal;
                $json_array[JsonElements::INTERPRETATION]["coding"][0]["display"] = "Normal";
            }

            //self::COMMENT
            //comment must have a value for FHIR, this is not required for Lincus, set null if it is "" in Lincus
            if ($comment === ""){
                $json_array[JsonElements::COMMENT] = "null";
            } else {
                $json_array[JsonElements::COMMENT] = $comment;
            }

            //self::RANGE
            //low
            $json_array[JsonElements::RANGE][0]["low"]["value"] = (float)$this->levels[$measurement_id]["low"];
            $json_array[JsonElements::RANGE][0]["low"]["unit"] = $this->getUnit($measurement_id)
            ["descriptions"][$this->getTermNumber($measurement_id)]["term"];
            $json_array[JsonElements::RANGE][0]["low"]["system"] = SNOMED_SYSTEM;
            $json_array[JsonElements::RANGE][0]["low"]["code"] = $this->units[$measurement_id]["code"];
            //high
            $json_array[JsonElements::RANGE][0]["high"]["value"] = (float)$this->levels[$measurement_id]["high"];
            $json_array[JsonElements::RANGE][0]["high"]["unit"] = $this->getUnit($measurement_id)
            ["descriptions"][$this->getTermNumber($measurement_id)]["term"];
            $json_array[JsonElements::RANGE][0]["high"]["system"] = SNOMED_SYSTEM;
            $json_array[JsonElements::RANGE][0]["high"]["code"] = $this->units[$measurement_id]["code"];
        }
        return $json_array;
    }

    /**
     * <h2>getFhirConversion</h2>
     * <br>
     * Converts the FHIR compliant message into a Lincus Compatible message. It populates the default
     * structure of the Lincus message with data from the FHIR message. This will then return the new message to
     * the calling function. It will return a measurement JSON message which will be used in the final message
     * to return to the Lincus platform.
     * @param $type int the type of measurement
     * @param $timestamp string the timestamp of the measurement
     * @param $value string the value of the measurement
     * @param $userid string the userid of the user the measurement was performed on
     * @param $user_details string value of the forename and surname of the user who wrote the data
     * @param $support_details string value of the forename and surname of the user who supports the subject
     * @param $editid string the editid of the user who performed the measurement
     * @param $device array the device that was used to record the measurement
     * @param $date string element of a date time value in the FHIR format
     * @param $comment string if a comment was entered in lincus, enter here
     * @return array the constructed measurement message in the Lincus structure
     * @internal param string $measurement_text the type of measurement in a text form
     * @internal param string $units the unit of the measurement. Example - kg, mmHg, steps
     */
    public function getFhirGetConversion($type, $timestamp, $value, $userid, $user_details, $support_details,
                                         $editid, $device, $date, $comment){
        //copy the default lincus message into a local variable
        $json_message = self::lincus_message;
        $json_message[JsonElements::TYPE] = (string)array_search($type, $this->measurements);

        $timestamp = str_replace("T", " ", $timestamp);
        $timestamp = substr($timestamp, 0, strpos($timestamp, "+"));
        $json_message[JsonElements::TIMESTAMP] = $timestamp;

        //remove the date from the date string and place the time into a new variable
        $json_message[JsonElements::DATE] = substr($date, 0, 10);
        $time = substr($date, 11, 18);
        //remove the timezone offset and the seconds to display into the Lincus message
        $json_message[JsonElements::TIME] =  substr($time, 0, sizeof($time)-7);

        //remove the timezone offset from the time variable
        $offset = substr($time, sizeof($time)-7);

        //if the timezone offset is UTC in a FHIR format, then change to equal 0
        if ($offset == "+00:00")
            $offset = "0";

        //place the timezone offset in timezone section of the lincus message
        $json_message[JsonElements::TIMEZONE] = $offset;

        //Place the value of the comment in the corresponding section if the comment != null
        if ($comment == "null")
            $json_message[JsonElements::COMMENTS] = "";
         else
            $json_message[JsonElements::COMMENTS] = $comment;


         //get the value of the measurement from the FHIR message
        $json_message[JsonElements::VALUE] = (string)$value;

        //Get the ID value from the FHIR structure and remove the string value leaving the integer
        $json_message[JsonElements::USER] = substr($userid, strpos($userid, "/")+1, (strlen($userid)));
        $json_message[JsonElements::EDIT] = substr($editid, strpos($editid, "/")+1, (strlen($editid)));

        //take the forename and the surname and place each into the corresponding section splitting on the space char
        //check if the index exists to ensure a forename and surname is present in the FHIR message
        if (array_key_exists("display", $user_details)) {
            $json_message[JsonElements::U_FORENAME] = substr($user_details["display"], 0, strpos($user_details["display"], " "));
            $json_message[JsonElements::U_SURNAME] = substr($user_details["display"], strpos($user_details["display"], " ") + 1,
                (strlen($user_details["display"])));
        }

        //take the support users forename and surname and place into corresponding section splitting on the space char
        //check if the index exists to ensure a forename and surname is present in the FHIR message
        if (array_key_exists("display", $user_details)) {
            $json_message[JsonElements::S_FORENAME] = substr($support_details["display"], 0, strpos($support_details["display"], " "));
            $json_message[JsonElements::S_SURNAME] = substr($support_details["display"], strpos($support_details["display"], " ") + 1,
                (strlen($support_details["display"])));
        }

        //If a device is present, save the device details into the Lincus message
        if (!is_null($device[0])) {
            $json_message[JsonElements::DEVICE_ID] = (int)$device[1];
            $json_message[JsonElements::DEVICE_NAME] = $device[0];
        }

        //return the new message structure to the calling function
        return $json_message;
    }


    /**
     * <h2>getTermNumber</h2>
     * <br>
     * The Response Message from SNOMED, contains many synonyms or terms of one definition.
     * This function, returns the integer value of the synonym that we want to display in the
     * FHIR JSON Message.
     * @param $measurement int value of the measurement which is in question
     * @return int value of the term number to get from the SNOMED response
     */
    private function getTermNumber($measurement){
        return $this->units[$measurement]['term_number'];
    }

    /**
     * <h2>accessSnomed</h2>
     * <br>
     * Access the URL that has been entered into the parameter $url. In most cases, the $url will access
     * SNOMED CT however, this method can be used to access any url that will return a json message to the
     * system. If, the system does not receive a response in a JSON form from the server, then the http_response_code
     * is set to 500 (Internal Server Error) and returned to the user.
     * <br>
     * This method also has a timeout set to the constant variable TIMEOUT at the top of this class. If the timeout is
     * hit, for any reason, then the value null will be returned to the calling function and will be handled accordingly
     * <br>
     * A copy of the JSON response from SNOMEDCT will be saved in a cache to minimise load times on the system.
     * This file will be deleted every 1 days to ensure that the data is kept correct and up to date.
     * @param $url String value of the url created previously
     * @param $sctid int value of the SCTID to use for the Cache file name
     * @param $searching_for string What data is being searched for? Used in the Log File
     * @return array JSON value from the SNOMEDCT API
     */
    private function accessSnomed($url, $sctid, $searching_for){
        //the name of the cached file
        $file_name = 'Cache/' . $sctid . '.json';

        //if there is a file cached, then read from there - it is faster
        if (file_exists($file_name)){
            $json_response = file_get_contents($file_name);
            $json_response = json_decode($json_response, true);
        }
        //otherwise, read from snomed itself and cache the result for 24 hours
        else {
            //initialise curl to access the SNOMED API
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);
            //set timeout to 3 seconds
            curl_setopt($ch, CURLOPT_TIMEOUT, TIMEOUT);

            //access the API
            $json_response = json_decode(curl_exec($ch), true);

            //if an error occurred eg: timeout get the number
            $curl_error_no = curl_errno($ch);
            $curl_error = curl_error($ch);
            curl_close($ch);

            //if an error occurred, report in log file.
            if ($curl_error_no > 0) {
                $this->error = true;
                http_response_code(408);
                self::logError("An error occurred with cURL: " . $curl_error . ". When accessing: " . $url);
            } else {
                //if there was no response, then an error must've occurred
                if (is_null($json_response)) {
                    $this->error = true;
                    //could not connect to snomed3
                    http_response_code(500); //Internal Server Error
                    header('HTTP/1.1 500 Internal Server Error: Could Not Connect To SNOMEDCT whilst searching for: ' . $searching_for);
                    self::logError('Could Not Connect to SNOMEDCT', $searching_for);
                } else if ($json_response === false){
                    //if an invalid SCTID is passed to SNOMEDCT, then it returns false. Check for this
                    $this->error = true;
                    http_response_code(500); //Internal Server Error
                    header('HTTP/1.1 500 Internal Server Error: Invalid SCTID found, please check array for: ' . $searching_for);
                    self::logError('Invalid SCTID found', $searching_for);
                } else{ //only create a file, if JSON is found
                    file_put_contents($file_name, json_encode($json_response));
                }
            }

            unset($ch);
        }
        //return the JSON from snomed
        return $json_response;
    }

    /**
     * <h2>checkFileTime</h2>
     * <br>
     * This function runs at the end of fetching all the relevant data from SNOMEDCT.
     * This function searches the directory "Cache" for any files that are older than
     * 1 day. If a files creation time is longer than a day, then it is deleted and
     * fetched again from SNOMEDCT, the next time data is requested with that SCTID.
     */
    private function checkFileTime(){
        //get the directory for the cached files
        $directory = glob('Cache/*');
        $time = time();

        //for every file, check it was created less than 24 hours ago. If it is too old, delete
        foreach ($directory as $file){
            if (is_file($file) && $file) {
                if (($time - filectime($file)) >= (60 * 60 * 24)) { //Seconds
                    unlink($file);
                }
            }
        }
    }

    /**
     * <h2>getSnomedMeasurementJSON</h2>
     * <br>
     * Sets the value of the url to search SNOMED CT for the corresponding measurement to SCTID stored in the
     * array $measurements.
     * @param $id int value to search for in the measurements array
     * @return array|null JSON Value from the SNOMEDCT API if key is found
     */
    private function getMeasurement($id){
        if (array_key_exists($id, $this->measurements)){
            $measurement = $this->measurements[$id];
            $url = BASE_URL . $measurement;
            return $this->accessSnomed($url, $measurement, 'measurement: ' . $id);
        }
        $error = 'No Measurement with that ID';
        $this->error = true;
        http_response_code(404);
        header("HTTP/1.1 404 Not Found: $error");
        self::logError($error, $id);
        return null;
    }

    /**
     * <h2>getUnit</h2>
     * <br>
     * Function to search through the units list and then search for the corresponding unit in the SNOMEDCT
     * API. Returns JSON value from the SNOMEDCT to calling method
     * @param $id int id number of the unit to fetch. Must be the same as the measurement using the unit
     * @return array|null JSON from SNOMEDCT if key is found
     */
    private function getUnit($id){
        if (array_key_exists($id, $this->units)){
            $unit = $this->units[$id]["code"];
            $url = BASE_URL . $unit;
            return $this->accessSnomed($url, $unit, 'unit: ' . $id);
        }
        $error = 'No Unit with that ID';
        $this->error = true;
        http_response_code(404);
        header("HTTP/1.1 404 Not Found: $error");
        self::logError($error, $id);
        return null;
    }

    /**
     * <h2>logError</h2>
     * <br>
     * This class logs all errors that are thrown during the execution of the Web Service. If an error occurs
     * during execution, then error is passed here with the causing value and it is
     * logged in the corresponding file. All log files are stored in the directory "Logs" and they
     * are ordered by date. Each file is named after the date that the error was thrown. Then,
     * the time that the error was caused is logged in the file with the relevant data.
     * <br>
     * This class also ensures that the file is created if it has not already been created.
     * @param $error string value of the error that has occurred and where
     * @param null $id_value int value of the value that has caused the error
     */
    public static function logError($error, $id_value = null){
        date_default_timezone_set("UTC");

        $time = time();
        $log_name = 'Logs/log-'.date('Y-m-d', $time).'.txt';
        $time = date('H:i:s', $time);

        if (!is_null($id_value)) {
            file_put_contents($log_name, "An error has occurred at: $time (+00:00). Error Message: $error. Value causing "
                . "error was: $id_value\r\n", FILE_APPEND);
        } else {
            file_put_contents($log_name, "An error has occurred at: $time (+00:00). Error Message: $error. \r\n",
                FILE_APPEND);
        }
    }
}