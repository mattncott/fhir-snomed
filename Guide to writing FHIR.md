Guide to Writing FHIR Compliant Message for Observations
========================================================
This is a guide to demonstrate how to generate a FHIR compliant JSON message using the Schemas provided by HL7.
All Schemas described as a $ref will be provided in this file to explain how to form the full message. 
###Full JSON Message - Observation Message
```json
{
  "resourceType" : {
    "description" : "What is the resource (in this case Observation)", 
    "type" : "string"
  },
  
  "id" : "string random, identifier for message",
  
  "text" : {
    "status" : "string, not really sure yet",
    "div" : "HTML Message containing the data"
  },
  
  "identifier" : {
    "description" : "A unique identifier assigned to this observation",
    "type" : "array",
    "items" : {
      "$ref" : "Identifier.schema.json#/definitions/Identifier"
    }
  },

  "status" : {
    "description" : "The status of the result value",
    "enum" : [
      "registered",
      "preliminary",
      "final",
      "amended",
      "corrected",
      "cancelled",
      "entered-in-error",
      "unknown"
    ],
    "type" : "string"
  },

  "code" : {
    "description" : "What was observed, This will be from SNOMED CT",
    "$ref" : "CodeableConcept.schema.json#/definitions/CodeableConcept"
  },

  "subject" : {
    "description" : "The patient, or group of patients, location, or device",
    "$ref" : "Reference.schema.json#/definitions/Reference"
  },

  "effectivePeriod" : {
    "description" : "The time or time-period the observed value is asserted as being true.",
    "$ref" : "Period.schema.json#/definitions/Period"
  },
  
  "issued" : {
    "description" : "The date and time this observation was made available to providers, typically after the results have been reviewed and verified",
    "type" : "string"
  },
  
  "performer" : {
    "description" : "Who was responsible for asserting the observed value as true",
    "type" : "array",
    "items" : {
      "$ref" : "Reference.schema.json#/definitions/Reference"
    }
  },

  "valueQuantity" : {
    "description" : "The information gathered by making this observation",
    "$ref": "Quantity.schema.json#/definitions/Quantity"
  },
  
  "interpretation" : {
    "description" : "The assessment made based on the result of the observation",
    "$ref" : "CodeableConcept.schema.json#/definitions/CodeableConcept"
  },
  
  "referenceRange" : {
    "description" : "Guidance on how to interpret the value by comparison to a normal or recommended range.",
    "type" : "array",
    "items" : {
      "$ref" : "#/definitions/Observation_ReferenceRange"
    }
  }
}
```

###Quantity Schema
"Quantity.schema.json#/definitions/Quantity"

```json
{
  "value" : {
    "description" : "Measured Amount or amount potentially measured",
    "type" : "integer",
    "pattern" : "-?([0]|([1-9][0-9]*))(\\.[0-9]+)?"
  },

  "unit" : {
    "description" : "readable form of the unit (EG: kg)",
    "type" : "string"
  },

  "system" : {
    "description" : "The identification of the system that provides the coded form of the unit",
    "type" : "string"
  },

  "code" : {
    "description" : "A computer processable form of the unit in some unit representation",
    "type" : "string",
    "pattern" : "[^\\s]+([\\s]?[^\\s]+)*"
  }
}
```

###CodeableConcept
"CodeableConcept.schema.json#/definitions/CodeableConcept"
```json
{
  "coding" : {
    "description" : "A reference to a code defined by a terminology system",
    "type" : "array",
    "items" : {
      "$ref" : "Coding.schema.json#/definitions/Coding"
    }
  }
}
```

###Coding
"Coding.schema.json#/definitions/Coding"
```json
{
  "system" : {
    "description" : "The identification of the code system that defines the meaning of the symbol in the code",
    "type" : "string"
  },

  "code" : {
    "description" : "A symbol in syntax defined by the system",
    "type" : "string",
    "pattern" : "[^\\s]+([\\s]?[^\\s]+)*"
  },

  "display" : {
    "description" : "A representation of the meaning of the code in the system, following rules of the system",
    "type" : "string"
  }
}
```

###Reference
"Reference.schema.json#/definitions/Reference"
```json
{
  "reference" : {
    "description" : "A reference to a location at which the other resource is found. The reference may be a relative reference, in which case it is relative to the service base URL, or an absolute URL that resolves to the location where the resource is found. The reference may be version specific or not. If the reference is not to a FHIR RESTful server, then it should be assumed to be version specific. Internal fragment references (start with \u0027#\u0027) refer to contained resources.",
    "type" : "string"
  },
  
  "display" : {
      "description": "Plain text narrative that identifies the resource in addition to the resource reference."
  }
}
```

###Identifier
"Identifier.schema.json#/definitions/Identifier"
```json
{
  "use" : {
    "description" : "The purpose of this identifier",
    "enum" : [
      "usual",
      "official",
      "temp",
      "secondary" 
    ]
  },
  "system" : {
    "description" : "Establishes the namespace for the value - that is, a URL that describes a set values that are unique",
    "type" : "string"
  },
  "value" : {
    "description" : "The portion of the identifier typically relevant to the user and which is unique within the context of the system",
    "type" : "string"
  }
}
```

###Observation_ReferenceRange
"#/definitions/Observation_ReferenceRange"
```json
{
  "low" : {
    "description" : "The value of the low bound of the reference range.",
    "$ref" : "Quantity.schema.json#/definitions/Quantity"
  },
  "high" : {
    "description" : "The value of the high bound of the reference range.",
    "$ref" : "Quantity.schema.json#/definitions/Quantity"
  }
}
```

###Period
"Period.schema.json#/definitions/Period"
```json
{
  "start": {
    "description": "The start of the period. The boundary is inclusive.",
    "type": "string",
    "pattern": "-?[0-9]{4}(-(0[1-9]|1[0-2])(-(0[0-9]|[1-2][0-9]|3[0-1])(T([01][0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9](\\.[0-9]+)?(Z|(\\+|-)((0[0-9]|1[0-3]):[0-5][0-9]|14:00)))?)?)?"
  },
  "end": {
    "description": "The end of the period. If the end of the period is missing, it means that the period is ongoing. The start may be in the past, and the end date in the future, which means that period is expected/planned to end at that time.",
    "type": "string",
    "pattern": "-?[0-9]{4}(-(0[1-9]|1[0-2])(-(0[0-9]|[1-2][0-9]|3[0-1])(T([01][0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9](\\.[0-9]+)?(Z|(\\+|-)((0[0-9]|1[0-3]):[0-5][0-9]|14:00)))?)?)?"
  }  
}
```