<?php
/**
 * <h1>Error_Page/h1>
 * <br>
 * This page displays all HTTP response errors that could occur when accesing the URL
 * from an outside application. Currently handles both 404 errors and 500 errors.
 * <br>
 * To add an error to this page to be caught and returned to calling application,
 * add error code and message to the URL below and add the error code and page to the
 * document .htaccess to redirect the user to this page.
 * <br>
 * This page will return a JSON response message corresponding to the Header Response
 * code that has occurred.
 * @author Matthew
 * @since 24/07/2017
 * @version 1.0
 */
require_once '../Encryption/Encrypt.php';

//See what error has occurred
$error_code = http_response_code();

//Holds descriptions of error codes that could occur when attempting to connect to the service
$error = array(
    302 => array(
        "error" => "HTTP Error 302: Found."
    ),
    400 => array(
        "error" => "HTTP Error 400: Bad Request."
    ),
    401 => array(
        "error" => "HTTP Error 401: You are not authorised to view this page."
    ),
    403 => array(
        "error" => "HTTP Error 403: You are not authorised to view this page."
    ),
    404 => array(
        "error" => "HTTP Error 404: The data requested at this url could not be found."
    ),
    408 => array(
        "error" => "HTTP Error 408: Request Timeout."
    ),
    500 => array(
        "error" => "HTTP Error 500: Internal Server Error."
    ),
    505 => array(
        "error" => "HTTP Error 505: Your HTTP Version is not supported."
    )
);

//Get the correct code from the array
$error = json_encode($error[$error_code]);

//encrypt it and send back to user
$error = Encrypt::encryptData($error);

echo $error;