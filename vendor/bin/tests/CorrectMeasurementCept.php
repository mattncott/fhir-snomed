<?php
$I = new ApiTester($scenario);
$I->wantTo('Correct 404 is returned if no measurement found');
$I->sendPOST(
    'http://snomed.test.dev/snomed/index.php?convert=lincus',
    file_get_contents("tests/_data/incorrect_measurements.json")
);
$I->seeResponseCodeIs(\Codeception\Util\HttpCode::NOT_FOUND); //404
$I->seeResponseMatchesJsonType([
    "resourceType" => "string",
    "type" => "string",
    "meta" => ([
        "lastUpdated" => "string",
    ]),
    "entry" => ([[
        "resource" => ([
            "resourceType" => "string",
            "id" => "string",
            "issue" => ([[
                "severity" => "string",
                "code" => "string",
                "details" => ([
                    "text" => "string"
                ])
            ]])
        ])
    ]])
]);