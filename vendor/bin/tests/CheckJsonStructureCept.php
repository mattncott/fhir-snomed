<?php
/**
 * <h1>CheckJsonStructureCept</h1>
 * This Unit Test, tests to ensure that the JSON response from the RESTful API
 * is in the correct format.
 * Ensure that the test case is enabled in index.php
 */
$I = new ApiTester($scenario);
$I->wantTo('JSON Response is Correct Structure');
$I->sendPOST(
    'http://snomed.test.dev/snomed/index.php?convert=lincus',
    file_get_contents("tests/_data/measurements.json")
);
$I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK); //200
$I->seeResponseIsJson();
//Checks the json is returned in the following format (when unencrypted)
$I->seeResponseMatchesJsonType([
    "resourceType" => "string",
    "type" => "string",
    "meta" => ([
       "lastUpdated" => "string"
    ]),
    "entry" => ([[
        "resource" => ([
            "resourceType" => "string",
            "id" => "string",
            "identifier" => ([[
                "use" => "string",
                "system" => "string",
                "value" => "string"
            ]]),
            "status" => "string",
            "code" => ([
                "coding" => ([
                    "system" => "string",
                    "code" => "string",
                    "display" => "string"
                ])
            ]),
            "subject" => ([
                "reference" => "string",
                "display" => "string"
            ]),
            "effectivePeriod" => ([
                "start" => "string"
            ]),
            "issued" => "string",
            "performer" => ([[
                "reference" => "string",
                "display" => "string"
            ]]),
            "valueQuantity" => ([
                "value" => "integer",
                "unit" => "string",
                "system" => "string",
                "code" => "string"
            ]),
            "interpretation" => ([
                "coding" => ([[
                    "system" => "string",
                    "code" => "string",
                    "display" => "string"
                ]])
            ]),
            "referenceRange" => ([[
                "low" => ([
                    "value" => "integer",
                    "unit" => "string",
                    "system" => "string",
                    "code" => "string"
                ]),
                "high" => ([
                    "value" => "integer",
                    "unit" => "string",
                    "system" => "string",
                    "code" => "string"
                ])
            ]]),
        ]),
        "fullUrl" => "string"
    ]])
]);