<?php
/**
 * This Unit Test, accesses the API with an incorrect measurement ID which should cause a 404 error to be
 * returned to the querying application.
 */
$I = new ApiTester($scenario);
$I->wantTo('Accessing the API with a non integer value for patient/author returns internal server error');
//Testing measurement 55, does not exist
$I->sendPOST(
    'http://snomed.test.dev/snomed/index.php?convert=lincus',
    file_get_contents("tests/_data/incorrect_user.json")
);
$I->seeResponseCodeIs(\Codeception\Util\HttpCode::INTERNAL_SERVER_ERROR); //500
$I->seeResponseIsJson();
$I->seeResponseMatchesJsonType([
    "resourceType" => "string",
    "type" => "string",
    "meta" => ([
        "lastUpdated" => "string"
    ]),
    "entry" => ([[
        "resource" => ([
            "resourceType" => "string",
            "id" => "string",
            "issue" => ([[
                "severity" => "string",
                "code" => "string",
                "details" => ([
                    "text" => "string"
                ])
            ]])
        ])
    ]])
]);