<?php 
$I = new ApiTester($scenario);
$I->wantTo('Validate the FHIR Message to Ensure structure is correct');
$I->sendPOST(
    'http://snomed.test.dev/snomed/index.php?convert=lincus',
    file_get_contents('tests/_data/measurements.json')
);
$I->sendPOST(
    'http://fhirtest.uhn.ca/baseDstu3/Bundle/$validate?_format=json&_pretty=true',
    $I->grabResponse()
);
$I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK); //200
