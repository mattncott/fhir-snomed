<?php 
$I = new ApiTester($scenario);
$I->wantTo('Malformed URL Returns Correct Header and JSON response');
$I->sendPOST("http://snomed.test.dev/snomed/index.php?convert=pineapple");
$I->seeResponseCodeIs(\Codeception\Util\HttpCode::NOT_FOUND); //404
$I->seeResponseMatchesJsonType([
    "resourceType" => "string",
    "id" => "string",
    "issue" => ([[
        "severity" => "string",
        "code" => "string",
        "details" => ([
            "text" => "string"
        ])
    ]])
]);