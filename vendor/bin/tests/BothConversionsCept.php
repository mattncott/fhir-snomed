<?php 
$I = new ApiTester($scenario);
$I->wantTo('Perform both conversions and see the data is still correct');
//Convert to FHIR
$I->sendPOST(
    'http://snomed.test.dev/snomed/index.php?convert=lincus',
    file_get_contents('tests/_data/measurements.json')
);
$I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
$I->seeResponseIsJson();
$I->seeResponseMatchesJsonType([
    "resourceType" => "string",
    "type" => "string",
    "meta" => ([
        "lastUpdated" => "string"
    ]),
    "entry" => ([[
        "resource" => ([
            "resourceType" => "string",
            "id" => "string",
            "identifier" => ([[
                "use" => "string",
                "system" => "string",
                "value" => "string"
            ]]),
            "status" => "string",
            "code" => ([
                "coding" => ([
                    "system" => "string",
                    "code" => "string",
                    "display" => "string"
                ])
            ]),
            "subject" => ([
                "reference" => "string",
                "display" => "string"
            ]),
            "effectivePeriod" => ([
                "start" => "string"
            ]),
            "issued" => "string",
            "performer" => ([[
                "reference" => "string",
                "display" => "string"
            ]]),
            "comment" => "string",
            "valueQuantity" => ([
                "value" => "integer",
                "unit" => "string",
                "system" => "string",
                "code" => "string"
            ]),
            "interpretation" => ([
                "coding" => ([[
                    "system" => "string",
                    "code" => "string",
                    "display" => "string"
                ]])
            ]),
            "referenceRange" => ([[
                "low" => ([
                    "value" => "integer",
                    "unit" => "string",
                    "system" => "string",
                    "code" => "string"
                ]),
                "high" => ([
                    "value" => "integer",
                    "unit" => "string",
                    "system" => "string",
                    "code" => "string"
                ])
            ]]),
        ]),
        "fullUrl" => "string"
    ]])
]);

//Convert the response back to Lincus Compliant data and check that
$I->sendPOST(
    'http://snomed.test.dev/snomed/index.php?convert=fhir',
    $I->grabResponse()
);
$I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
$I->seeResponseIsJson();
$I->seeResponseMatchesJsonType([
    "data" => ([
        "measurements" => ([[
            "type" => "string",
            "value" => "string",
            "timestamp" => "string",
            "date" => "string",
            "time" => "string",
            "timezone_offset" => "string",
            "comments" => "string",
            "userID" => "string",
            "user_forename" => "string",
            "user_surname" => "string",
            "editID" => "string",
            "support_forename" => "string",
            "support_surname" => "string",
            "device_id" => "integer",
            "device_name" => "string",
        ]])
    ]),
]);