<?php 
$I = new ApiTester($scenario);
$I->wantTo('Check I get the correct Error when using Malformed JSON');
$I->sendPOST(
    'http://snomed.test.dev/snomed/index.php?convert=lincus',
    file_get_contents("tests/_data/malformed.json")
);
$I->seeResponseCodeIs(\Codeception\Util\HttpCode::INTERNAL_SERVER_ERROR); //500
$I->seeResponseMatchesJsonType([
    "resourceType" => "string",
    "id" => "string",
    "issue" => ([[
        "severity" => "string",
        "code" => "string",
        "details" => ([
            "text" => "string"
        ])
    ]])
]);