<?php 
$I = new ApiTester($scenario);
$I->wantTo('Convert Data from FHIR format into a Lincus Format that can be read');
$I->sendPOST(
    'http://snomed.test.dev/snomed/index.php?convert=fhir',
    file_get_contents("tests/_data/fhir.json")
);
$I->seeResponseCodeIs(200);
$I->seeResponseIsJson();
$I->seeResponseMatchesJsonType([
    "data" => ([
        "measurements" => ([[
            "type" => "string",
            "value" => "string",
            "timestamp" => "string",
            "date" => "string",
            "time" => "string",
            "timezone_offset" => "string",
            "comments" => "string",
            "userID" => "string",
            "user_forename" => "string",
            "user_surname" => "string",
            "editID" => "string",
            "support_forename" => "string",
            "support_surname" => "string",
            "device_id" => "integer",
            "device_name" => "string",
        ]])
    ]),
]);